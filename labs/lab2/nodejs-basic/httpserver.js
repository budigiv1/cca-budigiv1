var http = require('http');

const fs = require('fs');
const url = require('url');

var server = http.createServer(httphandler);
var port = process.env.PORT || 8080;
server.listen(port);
console.log('httpserver is running on port: ' + port);

const querystring = require('querystring');
function httphandler(request, response) {
    console.log('Get an HTTP request: ' + request.url);
    var fullpath = url.parse(request.url, true);
    var localfile = '..' + fullpath.pathname;

    if (fullpath.pathname === '/echo.php') {
        if (request.method == 'GET') {
            var query = querystring.parse(querystring.stringify(fullpath.query));
            response.writeHead(200, { 'Content-Type': 'text/html' });
            response.write(query.data);
            return response.end();
        }
        else if (request.method == 'POST') {
            let postdata = '';
            request.on('data', (chunk) => {
                postdata += chunk;
            });
            request.on('end', () => {
                postdata = querystring.parse(postdata);
                response.writeHead(200, { 'Content-Type': 'text/html' });
                response.write(postdata.data);
                return response.end();
            });

        }
    } else {
        fs.readFile(localfile, (error, filecontent) => {
        if (error) {
            response.writeHead(404);
            return response.end(fullpath.pathname + ' is not found on this server.');
        }
        response.writeHead(200, { 'Content-Type': 'text/html' });
        response.write(filecontent);
        return response.end();
    });
    }
    return;
}